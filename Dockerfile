FROM ubuntu:bionic
LABEL maintainer="Sacha Ligthert <sacha@ligthert.net>"

RUN apt-get update; apt-get upgrade; apt-get install -y python3.8 python3-pip git
RUN pip3 install setuptools awscli aws-sam-cli

RUN mkdir /workspace
VOLUME /workspace
WORKDIR /workspace

RUN mkdir $HOME/.aws
VOLUME $HOME/.aws

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

#ENTRYPOINT /usr/local/bin/sam
